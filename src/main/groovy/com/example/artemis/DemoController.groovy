package com.example.artemis

import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Post

import javax.inject.Inject

@Controller("/api")
class DemoController {

    @Inject
    TextProducer textProducer

    @Post("/")
    HttpResponse demo(@Body String body) {
        textProducer.send(body)
        return HttpResponse.ok(body)
    }
}

