package com.example.artemis

import io.micronaut.jms.annotations.JMSListener
import io.micronaut.jms.annotations.Queue
import io.micronaut.messaging.annotation.Body

import static io.micronaut.jms.activemq.artemis.configuration.ActiveMqArtemisConfiguration.CONNECTION_FACTORY_BEAN_NAME

@JMSListener(CONNECTION_FACTORY_BEAN_NAME)
class TextConsumer {

    List<String> messages = [].asSynchronized()

    @Queue(value = 'queue_text', concurrency = '1-5')
    void receive(@Body String body) {
        println(body)
        messages << body
    }
}