package com.example

import com.example.artemis.DemoController
import io.micronaut.test.extensions.spock.annotation.MicronautTest
import org.apache.activemq.artemis.core.server.ActiveMQServer
import org.apache.activemq.artemis.core.server.impl.ActiveMQServerImpl
import spock.lang.Shared
import spock.lang.Specification
import org.apache.activemq.artemis.core.config.Configuration
import org.apache.activemq.artemis.core.config.impl.ConfigurationImpl

import javax.inject.Inject

@MicronautTest
class DemoControllerSpec extends Specification {

    @Shared
    ActiveMQServer server

    @Inject
    DemoController demoController

    void setup() {
        Configuration config = new ConfigurationImpl()
        config.addAcceptorConfiguration("in-vm", "vm://0")
        //config.addAcceptorConfiguration("tcp", "tcp://127.0.0.1:61616")
        server = new ActiveMQServerImpl(config)
        server.start()

    }

    void 'is server active'() {
        expect:
        server.active
    }

    void 'test endpoint'() {
        demoController.demo("test")
    }
}
